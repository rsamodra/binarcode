const express = require("express");

const tollRouter = express.Router();

tollRouter.use(function (req, res, next) {
  let saldo = parseInt(req.query.saldo === undefined ? 0 : req.query.saldo);
  if (saldo < 9000) {
    res.status(403).send("Saldo tidak cukup");
    return;
  }

  next();
});

tollRouter.get("/dalam-kota", function (req, res) {
  res.setHeader("Content-Type", "text/html");
  res.status(200);
  res.send("Anda masuk ke Dalam Kota");
});

tollRouter.get("/jorr", function (req, res) {
  res.setHeader("Content-Type", "text/html");
  res.status(200);
  res.send("Anda masuk ke Jakarta Outer Ring Road");
});

module.exports = tollRouter;
