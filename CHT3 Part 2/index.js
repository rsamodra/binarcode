const express = require("express");
const morgan = require("morgan");
const app = express();
const port = 8000;

app.use(function (req, res, next) {
  console.log(`${req.method} ${req.url}`);
  res.setHeader("X-Masuk-Middleware", true);
  next();
});

app.use(morgan("dev"));

const tollRouter = require("./toll-router");
app.use("/toll", tollRouter);

app.get("/ini-error", function (req, res) {
  iniError;
});

app.get("/", function (req, res) {
  res.setHeader("Content-Type", "text/html");
  res.status(200);
  res.send("Hello Binar");
});

app.get("/hello", function (req, res) {
  let name = req.query.name;
  res.status(200);
  res.send(`Hello ${name}`);
});

app.get("/json", (req, res) =>
  res.json({
    name: "Harits",
    job: "Software Engineer",
  })
);

app.get("/download", (req, res) =>
  res.sendFile("/Users/harits/Binar/Chapter 5/express/file.txt")
);

app.use(function (err, req, res, next) {
  console.error(err);
  res.status(500).send("Ada error di server. Mohon kontak tim IT");
});

app.use(function (err, req, res, next) {
  res.status(404).send("Nyasar nih?");
});

app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));
