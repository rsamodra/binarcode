const express = require("express");
const app = express();
const port = 8000;

app.get("/", function (req, res) {
  res.status(200);
  res.send("<h1>Hello World!</h1>");
});

app.get("/product", function (req, res) {
  res.status(200);
  res.send(["Apple", "Redmi", "One Plus One"]);
});

app.get("/order", function (req, res) {
  res.status(200);
  res.json([
    {
      id: 1,
      paid: false,
      user_id: 1,
    },
    {
      id: 2,
      paid: false,
      user_id: 1,
    },
  ]);
});

app.get("/user", (req, res) =>
  res.json({
    name: req.query.name,
    job: req.query.job,
    address: `Tinggal di ${req.query.address}`,
  })
);

app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));
