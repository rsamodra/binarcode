const express = require("express");
const app = express();
const port = 8000;

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  let name = req.query.name || "Anon";
  let job = req.query.job || "Unknown";
  res.render("index", { name, job });
});

app.get("/register", (req, res) => res.render("registration"));

app.post("/register", (req, res) => {
  const { name, job, address } = req.body;
  // Sama seperti
  // const name = req.body.name;
  // const job = req.body.job;
  // const address = req.body.address;

  res.render("registration", { name, job, address });
});

app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));
