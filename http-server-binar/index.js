const http = require("http");
const url = require("url");
const fs = require("fs");

const server = http.createServer((request, response) => {
  //   let parsedUrl = url.parse(request.url);
  //   let htmlPage = "";
  //   if (parsedUrl.pathname == "/home") {
  //     htmlPage = "./index.html";
  //   } else if (parsedUrl.pathname == "/hola") {
  //     htmlPage = "./hola.html";
  //   } else {
  //     fs.readFile("./404.html", null, (error, data) => {
  //       response.writeHead(404, {
  //         "Content-Type": "text/html",
  //       });
  //       response.write(data);
  //       response.end();
  //     });
  //     return;
  //   }

  //   fs.readFile(htmlPage, null, (error, data) => {
  //     if (error) {
  //       response.writeHead(404);
  //       response.write("File not found");
  //       response.end();
  //       return;
  //     }

  //     response.writeHead(200, {
  //       "Content-Type": "text/html",
  //       "X-Om-Telolet-Om": "telolet",
  //     });
  //     response.write(data);
  //     response.end();
  //   });

  // localhost:8000/users?name="Eren"&job="Founding Titan"
  // {"name": "Eren"}
  let parsedUrl = url.parse(request.url, true);
  response.writeHead(200, { "Content-Type": "application/json" });
  let data = {
    name: parsedUrl.query.name,
    job: parsedUrl.query.job,
  };
  response.write(JSON.stringify(data));
  response.end();
});
server.listen(8000);
