const http = require('http');
const fs = require('fs');
const url = require('url');

function lemparData(request, response) {
    const lemparUrl = url.parse(request.url, true);

    response.writeHead(200, { "Content-Type": "application/json" });

    let dataKu = {
        nama: lemparUrl.query.name,
        job: lemparUrl.query.job
    };

    response.write(JSON.stringify(dataKu));
    response.end();
}

http.createServer(lemparData).listen(8000);