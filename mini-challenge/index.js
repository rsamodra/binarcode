const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.status(200);
    res.send('halo ini home mini challenge dengan express');
});

app.get('/fibonacci', (req, res) => {
    let limit = req.query.limit;
    let hasil = cetakFibonExpress(limit);
    res.status(200);
    res.send(JSON.stringify(hasil))
});

function cetakFibonExpress(nilai) {
    let a = 0, b = 1, hasil = b, deretFibon = [];
    for (let index = 0; index < nilai; index++) {
        deretFibon.push(hasil);
        hasil = a + b;
        a = b;
        b = hasil;
    }
    return deretFibon;
}

app.listen(port, () => console.log(`listening at port ${port}...`));
