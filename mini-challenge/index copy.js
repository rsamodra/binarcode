const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.status(200);
    res.send('halo ini home mini challenge dengan express');
});

app.get('/fibonacci', (req, res) => {
    let limit = req.query.limit;
    let hasil = cetakFibonExpress(limit);
    res.status(200);
    res.send(JSON.stringify(hasil))
});

function cetakFibonExpress(nilai) {
    let a = 0, b = 1, hasil = b, deretFibon = [];
    for (let index = 0; index < nilai; index++) {
        deretFibon.push(hasil);
        hasil = a + b;
        a = b;
        b = hasil;
    }
    return deretFibon;
}

app.listen(port, () => console.log(`listening at port ${port}...`));

const http = require('http');
const url = require('url');

const server = http.createServer(function (request, respond) {

    respond.writeHead(200, { 'Content-Type': 'text/html' });

    if (request.url === '/') {
        respond.write('halo ini home mini challenge');
        respond.end();
    } else if (request.url.includes('/fibonancy')) {
        // format required http://localhost:8000/fibonancy?limit=

        let perulangan = url.parse(request.url, true);
        let cekLimit = perulangan.query.limit;

        if (cekLimit < 3) {
            respond.writeHead(400, { "Content-Type": "text/html" });
            respond.end('limit tidak boleh kurang dari 3!');
        } else {
            let hasil = cetakFibon(cekLimit);

            respond.writeHead(200, { "Content-Type": "text/html" });
            respond.write(JSON.stringify(hasil));
            respond.end();
        }

    } else {
        respond.writeHead(404, { "Content-Type": "text/html" });
        respond.write('404');
        respond.end;
    }

    function cetakFibon(nilai) {
        let a = 0, b = 1, hasil = b, deretFibon = [];

        for (let index = 0; index < nilai; index++) {
            deretFibon.push(hasil);
            hasil = a + b;
            a = b;
            b = hasil;
        }
        return deretFibon;

    }
});

// server.listen(8000);