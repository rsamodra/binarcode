const express = require('express');
const app = express();
const port = 8000;

let posts = require('./posts.json');

app.get('/api/posts', (req, res) => {
    res.status(200).json(posts);
});

app.get('/api/posts/:id', (req, res) => {
    const post = posts.find(function (post) {
        return post.id === parseInt(req.params.id);
    })

    if (!post) {
        res.status(404);
        // res.json({});
        res.send('404 error');
        return;
    }

    res.status(200);
    res.json(post);
})

// menjadikan server membaca body
app.use(express.json());

app.post('/api/posts', (req, res) => {
    const { title, body } = req.body;

    if (!title) {
        res.status(400).json({ 'Message': 'title can not blank' });
        return;
    }
    if (!body) {
        res.status(400).json({ 'Message': 'body can not blank' });
    }

    const id = posts[posts.length - 1].id + 1;

    const post = { id, title, body };
    posts.push(post);

    res.status(201)
    res.json(post);
});

app.put('/api/posts/:id', (req, res) => {
    //mencari post berdasar id
    let post = posts.find(function (post) {
        return post.id === +req.params.id;
    });

    //ganti data di object dengan nilai dari body put
    post.title = req.body.title;
    post.body = req.body.body;

    //ganti object existing dengan data modifikasi

    posts = posts.map(function (currentPost) {
        if (currentPost.id === +req.params.id) {
            return post;
        }
        return currentPost;
    })

    //kirim hasil ke halaman
    res.status(200);
    res.json(post);
})

app.delete('/api/posts/:id', (req, res) => {
    //penampungan array sementara
    posts = posts.filter(function (post) {
        return post.id !== +req.params.id;
    });

    res.status(200);
    res.json({
        message: `data ID ${req.params.id} deleted...`
    });
})

app.listen(port, () => console.log(`ready at port ${port}`));