const express = require('express');
const app = express();
const port = 3000;

app.use(function (req, res, next) {
    console.log(req.url);
    next();
});

const routerKU = express.Router();

routerKU.use(function (req, res, next) {
    let saldo = parseInt(req.query.saldo === undefined ? 0 : req.query.saldo);

    if (saldo < 9000) {
        res.status(403).send("saldo tidak cukup");
        return;
    }
    next();
});

routerKU.get("/dalam-kota", function (req, res) {
    res.status(200);
    res.send('selamat dalam kota');
});

app.use(routerKU);


app.get('/', function (req, res) {
    res.status(200);
    res.send('testing');
});

app.get('/cetak', function (req, res) {
    let nama = req.query.nama;
    res.send(`testing ${nama}`);
});

app.get('/product', (req, res) => {
    res.status(200)
    res.send(["Apple", "Redmi", "One Plus One"]);
})

app.get('/order', (req, res) => {
    res.status(200)
    res.json(
        [
            { "id": 1, "paid": false, "user_id": 1 },
            { "id": 2, "paid": false, "user_id": 2 }
        ]);
})

app.listen(port, () => console.log(`koneksi sukses di port ${port}`));