const http = require('http');
const fs = require('fs');

// function onRequest(request, response) {
//     fs.readFile("./index.html", null, function (error, data) {
//         if (error) {
//             response.writeHead(404);
//             response.write("file not found");
//             response.end();
//             return;
//         }

//         response.write(data);
//         response.end();
//     });
// }

function onRequest(request, response) {
    response.writeHead(200, { "Content-Type": "application/json" })
    const data = {
        nama: "AkuAku",
        umur: 10
    }
    // response.write(JSON.stringfy(data));
    response.end(JSON.stringify(data));
}

// http.createServer(onRequest).listen(8000);
http.createServer(onRequest).listen(8000);